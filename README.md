# Raspberry Pi Zero-W Cross Compiling Setup
This document is intended to help guide the reader through the process of setting up a cross compiling environment for use with a Raspberry Pi Zero-W.
The environment will be running on Ubuntu 20.04LTS using CodeLite as the IDE.

## Ubuntu 20.04 LTS Installation
There are many tutorials on how to install Ubuntu so this is not included. Use the link below to download and install

[Ubuntu 20.04 LTS](https://ubuntu.com/download/desktop)

## Raspberry Pi Zero-W Setup
### Hardware and Physical Requirements
1. A Raspberry Pi Zero W:
[Raspberry Pi Zero W](https://www.raspberrypi.org/products/raspberry-pi-zero-w/)

2. A 16Gb Micro-SD card, Class 10 an a minimum is recommended.

3. A 1.3" OLED with I2C interface. 

4. 7 male to male jumper wires. 

5. 2x 4.7k resistors.

6. A USB to serial adapter.

### Hardware setup
1. Solder the 40pin header to the Raspberry Pi Zero-W. The normal orientation for the header is with the pins facing out of the component side of the PCB. Clean off any flux residue when finished.

2. Connect the USB to Serial adapter to the 40 pin header using male to male jumper wires

3. Connect the USB to Serial adapter to a USB port on your PC

4. Solder a 4.7k resitor between 3.3V and SDA pins on the front of the display. Solder a 4.7k resistor between the 3.3V and SCLK pins on the front of the display

5. Connect the OLED to the RaspberryPi Zero-W 40 pin header using male to male jumper wires.

### OS Installation
1. Download the latest Raspberry Pi OS (buster) with desktop and recommended software.

2. Use the Ubunutu "Startup Disk Creator" to copy the downloaded image to the SD card.

3. Enable the Raspberry Pi serial port. Use the file manager to open the "boot" partition on the SD card. Open the "config.txt" file with a text editor and add "enable_uart=1" at the bottom (last line) of the file and save.

4. Unmount the SD card.

5. Insert the SD card into the Raspberry Pi Zero-W SD card slot.

6. Find the serial adaptor address. Open and terminal on the PC and type "dmesg | grep tty". The USB device will be similar to "ttyUSB0".

7. Execute Minicom with the following command "minicom -D /dev/ttyUSB0" this will start a session to monitor the serial port of the Rasperry Pi Zero.

8. Power up the raspberry Pi Zero by connecting the micro-USB cable.

9. Monitor the start-up process with the minicom session. Login when prompted with user "pi" password "raspberry'

10. At the command prompt (minicom) type sudo raspi-config. Enable WiFi (SSID and password) SSH, I2C and SPI interfaces

11. Set up a static IP on the wireless network interface by typing "sudo nano /etc/dhcpcd.conf". Add the following lines:

interface wlan0
static ip_address=192.168.2.39/24
static routers=8.8.8.8
static domain_name_servers=8.8.4.4 8.8.8.8

12. Reboot the Raspberry Pi by typing "sudo reboot"

13. Login to the minicom sesson and check the WAN configuration by typing "ifconfig"

14. Open another terminal window and type "ssh pi@192.168.2.39" to connect to the raspberry pi with SSH. Enter the password "raspberry'. Ignore any warnings about hte default password being used.

15. Update the upgrade the Raspberry Pi OS with "sudo apt-get update" and then "sudo apt-get upgrade"

16. Install wiringPi and i2c tools with the following commands. "sudo apt-get install wiringpi" , "sudo apt-get install -y i2c-tools"

17. Check the i2cdetect tool to see if the OLED display is visible on the I2C bus with "sudo i2cdetect -y 1". The 3C device should be visible.

## Setup the Ubuntu software
1. Create a project folder with a "prog_lib_links: sub folder. "cd" into the folder and sync files from the Rasperry Pi with "rsync -rzLR --safe-links pi@192.168.2.39:/usr/lib/libw* pi@192.168.2.39:/usr/lib/arm-linux-gnueabihf pi@192.168.2.39:/usr/lib/gcc/arm-linux-gnueabihf pi@192.168.2.39:/usr/include pi@192.168.2.39:/lib/arm-linux-gnueabihf sysroot/ "

2. Clone the linked repository into the following directoy: /home/william/Desktop/DSC/RPi-zero-programming3
git clone https://will_hanna@bitbucket.org/will_hanna/rpi-rotary-encoder.git

3. Install Codelite: 
sudo apt-key adv --fetch-keys http://repos.codelite.org/CodeLite.asc
open /etc/apt/sources.list in an editor (as superuser) and append the proper line
from Table 1
Table 1 entry: deb https://repos.codelite.org/ubuntu2/ xenial universe
sudo apt-get update
sudo apt-get install codelite

4. Download the RPi toolchain and install: https://github.com/Pro/raspi-toolchain

5. Open Codelite and add the RPi compiler

6. Open the RPi-zero-programming3 workspace

7. Build project

## Codelite project settings for new projects
1. Add the following paths to the Compiler Paths:
.
/home/william/Desktop/DSC/RPi-zero-programming/prog_lib_links/sysroot/usr/lib
/home/william/Desktop/DSC/RPi-zero-programming/prog_lib_links/sysroot/lib/arm-linux-gnueabihf/
/home/william/Desktop/DSC/RPi-zero-programming/prog_lib_links/sysroot/usr/include

2. Add the following library search paths to the linker:
.
/home/william/Desktop/DSC/RPi-zero-programming/prog_lib_links/sysroot/usr/lib

3. Add the wiringPi library to the llinker Libraries "wiringPi"

4. Setup an external tool for copying and running the binary to the RPi:
Name: SCP RPi
Tool path: sshpass -p "raspberry" scp -r $(ProjectName) pi@192.168.2.39:/home/pi/projects/test_progs/;sshpass -p "raspberry"  ssh pi@192.168.2.39 /home/pi/projects/test_progs/$(ProjectName) 
Working Directory:
$(ProjectPath)/Debug
Toolbar icon - to whatever
Enable the "Capture process output" tick box
